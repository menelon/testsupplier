<?php

print_r($_ENV);

if (!is_dir('/result')) {
    exit("Result dir is not mounted");
}

$result = do_some_work();

file_put_contents('/result/' . date('YmdHis') . '.txt', $result);

function do_some_work()
{
    $data = [];
    for ($i = 0; $i < 5; $i++) {
        $data[] = $i;
        sleep(1);
    }

    return implode("\n", $data);
}

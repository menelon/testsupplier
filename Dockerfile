FROM php:7.2.11-cli-alpine3.8
COPY . /app
WORKDIR /app
CMD [ "php", "./src/parser.php" ]
